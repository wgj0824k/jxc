package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService ;


    /**
     * 分页带条件查询客户
     *
     * @param page 当前页码
     * @param rows 每页行数
     * @param customerName  客户名
     * @return map
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows,
                                   @RequestParam(value = "customerName", required = false) String customerName) {

        if(StringUtil.isEmpty(customerName)) {
            return customerService.findCustomerListByPage(page , rows);
        }else {
            return customerService.findCustomerList(page , rows , customerName);
        }
    }

    /**
     * 客户添加或修改
     * @param customerId 客户id
     * @return result对象
     */
    @PostMapping(value = "/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "customerId",required = false) Integer customerId ,
                                  Customer customer) {

        if(customerId == null) {                // 保存
            customerService.save(customer) ;
            return new ServiceVO(100 , "添加成功" ) ;
        }else {                                 // 更新
            customerService.update(customer) ;
            return new ServiceVO(100 , "修改成功" ) ;
        }
    }

    /**
     * 删除客户（支持批量删除）
     *
     * @param ids
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("ids") String ids) {
        customerService.deleteBatch(ids) ;
        return new ServiceVO(100 , "删除成功" ) ;
    }
}
