package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.util.StringUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @description 商品信息Controller
 */
@RestController
@RequestMapping("/goods")
public class GoodsController {

    @Autowired
    private GoodsService goodsService;

    /**
     * 分页查询商品库存信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param codeOrName  商品编码或名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/listInventory")
    public Map<String, Object> findGoodsList(@RequestParam("page") Integer page,
                                             @RequestParam("rows") Integer rows,
                                             @RequestParam(value = "codeOrName", required = false) String codeOrName,
                                             @RequestParam(value = "goodsTypeId", required = false) String goodsTypeId) {
        if (StringUtil.isEmpty(codeOrName) && StringUtil.isEmpty(goodsTypeId)) {
            return goodsService.findGoodsListByPage(page, rows);
        } else {
            return goodsService.findGoodsList(page, rows, codeOrName, goodsTypeId);
        }
    }

    /**
     * 分页查询商品信息
     * 采取mybatis的分页插件pageHelper
     * @param page 当前页
     * @param rows 每页显示条数
     * @return
     */
//    @PostMapping("/listInventory")
//    public Map<String,Object>  findGoodsListByPage(@RequestParam("page") Integer page ,
//                                                  @RequestParam("rows") Integer rows) {
//        return goodsService.findGoodsListByPage(page , rows);
//    }

    /**
     * 生成商品编码
     *
     * @return
     */
    @RequestMapping("/getCode")
    @RequiresPermissions(value = "商品管理")
    public ServiceVO getCode() {
        return goodsService.getCode();
    }

    /**
     * 分页查询商品信息
     *
     * @param page        当前页
     * @param rows        每页显示条数
     * @param goodsName   商品名称
     * @param goodsTypeId 商品类别ID
     * @return
     */
    @PostMapping("/list")
    public Map<String, Object> findGoods(@RequestParam("page") Integer page,
                                         @RequestParam("rows") Integer rows,
                                         @RequestParam(value = "goodsName", required = false) String goodsName,
                                         @RequestParam(value = "goodsTypeId", required = false) String goodsTypeId) {
        if (StringUtil.isEmpty(goodsName) && StringUtil.isEmpty(goodsTypeId)) {
            return goodsService.findGoodsListByPage(page, rows);
        } else {
            return goodsService.findGoods(page, rows, goodsName, goodsTypeId);
        }
    }

    /**
     * 商品添加或修改
     *
     * @param goodsId 商品id
     * @return result对象
     */
    @PostMapping(value = "/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "goodsId", required = false) Integer goodsId,
                                  Goods goods) {

        if (goodsId == null) {                // 保存
            goodsService.save(goods);
            return new ServiceVO(100, "添加成功");
        } else {                                 // 更新
            goodsService.update(goods);
            return new ServiceVO(100, "修改成功");
        }
    }

    /**
     * 商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品id
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("goodsId") Integer goodsId) {
        goodsService.delete(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * @param page       当前页
     * @param rows       每页行数
     * @param nameOrCode 商品名称和编号
     * @return
     */
    @PostMapping("/getNoInventoryQuantity")
    public Map<String, Object> getNoInventoryQuantity(@RequestParam("page") Integer page,
                                                      @RequestParam("rows") Integer rows,
                                                      @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {

        if (StringUtil.isEmpty(nameOrCode)) {
            return goodsService.getNoInventoryQuantity(page, rows);
        } else {
            return goodsService.getNoInventoryQuantityByNameOrCode(page, rows, nameOrCode);
        }
    }

    /**
     * @param page       当前页
     * @param rows       每页行数
     * @param nameOrCode 商品名称和编号
     * @return
     */
    @PostMapping("/getHasInventoryQuantity")
    public Map<String, Object> getHasInventoryQuantity(@RequestParam("page") Integer page,
                                                       @RequestParam("rows") Integer rows,
                                                       @RequestParam(value = "nameOrCode", required = false) String nameOrCode) {

        if (StringUtil.isEmpty(nameOrCode)) {
            return goodsService.getHasInventoryQuantity(page, rows);
        } else {
            return goodsService.getHasInventoryQuantityByNameOrCode(page, rows, nameOrCode);
        }
    }

    /**
     * 添加库存、修改数量或成本价
     *
     * @param goodsId 商品id
     * @param inventoryQuantity 期初库存数量
     * @param purchasingPrice 成本价格（采购价格）
     * @return
     */
    @PostMapping(value = "/saveStock")
    public ServiceVO saveOrUpdate(@RequestParam(value = "goodsId") Integer goodsId,
                                  @RequestParam(value = "inventoryQuantity") Integer inventoryQuantity,
                                  @RequestParam(value = "purchasingPrice") Double purchasingPrice) {

            goodsService.saveStockOrPurchasingPrice(goodsId ,inventoryQuantity , purchasingPrice);
            return new ServiceVO(100, "添加库存成功");
    }

    /**
     * 库存删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     *
     * @param goodsId 商品id
     * @return
     */
    @PostMapping("/deleteStock")
    public ServiceVO deleteStock(@RequestParam("goodsId") Integer goodsId) {
        goodsService.deleteStock(goodsId);
        return new ServiceVO(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS);
    }

    /**
     * 查询所有 当前库存量 小于 库存下限的商品信息
     *
     * @return map
     */
    @PostMapping("/listAlarm")
    public Map<String,Object> listAlarm() {
        return goodsService.listAlarm() ;
    }

}
