package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import com.atguigu.jxc.util.StringUtil;
import com.atguigu.jxc.util.result.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/supplier")
public class SupplierController {

    @Autowired
    private SupplierService supplierService ;


    /**
     * 分页带条件查询供应商
     *
     * @param page 当前页码
     * @param rows 每页行数
     * @param supplierName  供应商名
     * @return map
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("page") Integer page,
                                   @RequestParam("rows") Integer rows,
                                   @RequestParam(value = "supplierName", required = false) String supplierName) {

        if(StringUtil.isEmpty(supplierName)) {
            return supplierService.findSupplierListByPage(page , rows);
        }else {
            return supplierService.findSuppliersList(page , rows , supplierName);
        }
    }

    /**
     * 供应商添加或修改
     * @param supplierId 供应商id
     * @return result对象
     */
    // 请求体：Content-Type: application/x-www-form-urlencoded，
    // 使用应用程序/x-www-form-urlencod编码时，Spring并不理解它是一个请求体
    // 简单来说，就是前后端数据交互的时候，出现json数据类型不匹配
    // 去掉@RequestBody注解
    @PostMapping(value = "/save")
    public ServiceVO saveOrUpdate(@RequestParam(value = "supplierId",required = false) Integer supplierId ,
                                  Supplier supplier) {

        if(supplierId == null) {                // 保存
            supplierService.save(supplier) ;
            return new ServiceVO(100 , "添加成功" ) ;
        }else {                                 // 更新
            supplierService.update(supplier) ;
            return new ServiceVO(100 , "修改成功" ) ;
        }
    }

    /**
     * 删除供应商（支持批量删除）
     *
     * @param ids 批量删除的id ids: 13,14
     * @return
     */
    @PostMapping("/delete")
    public ServiceVO delete(@RequestParam("ids") String ids) {
        supplierService.deleteBatch(ids) ;
        return new ServiceVO(100 , "删除成功" ) ;
    }
}
