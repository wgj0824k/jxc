package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.service.DamageListGoodsService;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/damageListGoods")
public class DamageListGoodsController {

    @Autowired
    private DamageListGoodsService damageListGoodsService ;

    /**
     * 保存报损单：涉及两张表：t_damage_list和t_damage_list_goods
     *
     * @param damageNumber 报损单号（一个报损单列表对应一个报损单号）
     * @param damageList 报损单列表实体类
     * @param damageListGoodsStr damageListGoods的json格式
     *
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(@RequestParam("damageNumber") String damageNumber ,
                        DamageList damageList,
                        @RequestParam("damageListGoodsStr") String damageListGoodsStr) {

        damageListGoodsService.save(damageNumber , damageList , damageListGoodsStr) ;
        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS) ;
    }

    /**
     * 报损单查询
     *
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("sTime") String sTime,
                                   @RequestParam("eTime") String eTime) {

        return damageListGoodsService.list(sTime , eTime) ;
    }

    /**
     * 查询报损单商品信息
     *
     * @param damageListId 报损单Id
     * @return map
     */
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(@RequestParam("damageListId") Integer damageListId) {

        return damageListGoodsService.goodsList(damageListId) ;
    }

}
