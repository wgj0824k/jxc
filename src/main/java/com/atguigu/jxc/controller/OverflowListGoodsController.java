package com.atguigu.jxc.controller;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/overflowListGoods")
public class OverflowListGoodsController {

    @Autowired
    private OverflowListGoodsService overflowListGoodsService ;

    /**
     * 保存报损单：涉及两张表：t_damage_list和t_damage_list_goods
     *
     * @param overflowNumber 报溢单号（一个报损单列表对应一个报损单号）
     * @param overflowList 报溢单列表实体类
     * @param overflowListGoodsStr damageListGoods的json格式
     *
     * @return
     */
    @PostMapping("/save")
    public ServiceVO save(@RequestParam("overflowNumber") String overflowNumber ,
                        OverflowList overflowList,
                        @RequestParam("overflowListGoodsStr") String overflowListGoodsStr) {

        overflowListGoodsService.save(overflowNumber , overflowList , overflowListGoodsStr) ;
        return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS) ;
    }

    /**
     * 报溢单查询
     *
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return map
     */
    @PostMapping("/list")
    public Map<String,Object> list(@RequestParam("sTime") String sTime,
                                   @RequestParam("eTime") String eTime) {

        return overflowListGoodsService.list(sTime , eTime) ;
    }

    /**
     * 查询报损单商品信息
     *
     * @param overflowListId 报溢单Id
     * @return map
     */
    @PostMapping("/goodsList")
    public Map<String,Object> goodsList(@RequestParam("overflowListId") Integer overflowListId) {

        return overflowListGoodsService.goodsList(overflowListId) ;
    }

}
