package com.atguigu.jxc.util.result;

import lombok.Getter;

/**
 * 统一返回结果状态信息类
 *
 */
@Getter
public enum ResultCodeEnum {

    SUCCESS(200,"成功"),
    FAIL(201, "失败"),
    SERVICE_ERROR(2012, "服务异常"),

    PAY_RUN(205, "支付中"),

    LOGIN_AUTH(208, "未登陆"),
    PERMISSION(209, "没有权限"),
    SECKILL_NO_START(210, "秒杀还没开始"),
    SECKILL_RUN(211, "正在排队中"),
    SECKILL_NO_PAY_ORDER(212, "您有未支付的订单"),
    SECKILL_FINISH(213, "已售罄"),
    SECKILL_END(214, "秒杀已结束"),
    SECKILL_SUCCESS(215, "抢单成功"),
    SECKILL_FAIL(216, "抢单失败"),
    SECKILL_ILLEGAL(217, "请求不合法"),
    SECKILL_ORDER_SUCCESS(218, "下单成功"),
    COUPON_GET(220, "优惠券已经领取"),
    COUPON_LIMIT_GET(221, "优惠券已发放完毕"),

    SYS_ERROR(999,"当前网络不佳，请稍后再试"),
    ERROR_SPU_REF(311, "被SPU所引用"),
    ERROR_SKU_REF(312, "被SKU所引用"),
    REDIS_DATA_ERROR(412, "请通过正常渠道进行访问"),
    LOGIN_ERROR(433 , "用户名或者密码错误!"),
    CART_ERROR(520 , "非法的购物车数量增减"),
    CART_ITEM_NUMBER_ERROR(521 , "购物车数量超限"),
    CART_NULL_ERROR(522 , "请通过正常渠道结算") ,
    ORDER_USERID_ERROR(9001 , "用户信息已失效"),
    ARGS_INVALIDE(9002 , "方法参数无效"),
    REQ_REPEAT(9003 , "请勿重复提交订单"),
    SKU_PRICE_ERROR(9004 , "商品价格已变更"),
    SKU_STOCK_ERROR(9005 , "正在加紧上货中")


    ;

    private Integer code;

    private String message;

    private ResultCodeEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
