package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.CustomerDao;
import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.CustomerService;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerDao customerDao ;

    // 分页客户列表展示
    @Override
    public Map<String, Object> findCustomerListByPage(Integer page, Integer rows) {

        HashMap<String, Object> map = new HashMap<>();

        List<Customer> customerList = customerDao.findCustomerListByPage(page , rows) ;

        map.put("total" , Long.toString(customerList.size())) ;
        map.put("rows" , customerList) ;
        return map;
    }

    // 分页带条件客户列表展示
    @Override
    public Map<String, Object> findCustomerList(Integer page, Integer rows, String customerName) {

        HashMap<String, Object> map = new HashMap<>();

        List<Customer> customerList = customerDao.findCustomerList(page , rows , customerName) ;

        map.put("total" , Long.toString(customerList.size())) ;
        map.put("rows" , customerList) ;
        return map;
    }

    // 保存
    @Override
    public void save(Customer customer) {
        customerDao.save(customer) ;
    }

    // 更新
    @Override
    public void update(Customer customer) {
        customerDao.update(customer) ;
    }

    // 删除（可批量）
    @Override
    public void deleteBatch(String ids) {
        //  处理ids (ids: 13,14)
        String[] idArr = ids.split(",");
        for (String idStr : idArr) {
            int id = Integer.parseInt(idStr);
            customerDao.deleteBatch(id);
        }

    }
}
