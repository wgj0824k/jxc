package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.dao.OverflowListDao;
import com.atguigu.jxc.dao.OverflowListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowList;
import com.atguigu.jxc.entity.OverflowListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import com.atguigu.jxc.service.OverflowListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OverflowGoodsServiceImpl implements OverflowListGoodsService {

    @Autowired
    private OverflowListGoodsDao overflowListGoodsDao ;
    @Autowired
    private OverflowListDao overflowListDao ;

    /**
     * 保存报损单：涉及两张表：t_damage_list和t_damage_list_goods
     *
     * @param overflowNumber 报损单号（一个报损单列表对应一个报损单号）
     * @param overflowList 报损单列表实体类
     * @param overflowListGoodsStr damageListGoods的json格式
     *
     * @return
     */
    @Override
    @Transactional
    public void save(String overflowNumber, OverflowList overflowList, String overflowListGoodsStr) {
        List<OverflowListGoods> overflowListGoods = JSON.parseArray(overflowListGoodsStr, OverflowListGoods.class);
        // 参数赋值
        // userId采取管理员1
        overflowList.setUserId(1);
        overflowList.setOverflowNumber(overflowNumber);

        // 分开两张表进行保存
        overflowListDao.save(overflowList) ;
        OverflowList overflowListSave = overflowListDao.findByOverflowNumber(overflowNumber) ;
        for (OverflowListGoods overflowListGood : overflowListGoods) {
            overflowListGood.setOverflowListId(overflowListSave.getOverflowListId());
            overflowListGoodsDao.save(overflowListGood) ;
        }
    }

    /**
     * 报溢单查询
     *
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
        List<OverflowList> overflowLists = overflowListDao.list(sTime , eTime) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows" , overflowLists) ;
        return map;
    }

    /**
     * 查询报溢单商品信息
     *
     * @param overflowListId 报溢单Id
     * @return map
     */
    @Override
    public Map<String, Object> goodsList(Integer overflowListId) {

        List<OverflowListGoods> overflowListGoodsList = overflowListGoodsDao.goodsList(overflowListId) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows" , overflowListGoodsList) ;
        return map;
    }
}
