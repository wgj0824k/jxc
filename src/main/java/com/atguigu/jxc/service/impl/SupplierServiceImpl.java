package com.atguigu.jxc.service.impl;

import com.atguigu.jxc.dao.SupplierDao;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Supplier;
import com.atguigu.jxc.service.SupplierService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SupplierServiceImpl implements SupplierService {
    @Autowired
    private SupplierDao supplierDao ;

    // 分页供应商列表展示
    @Override
    public Map<String, Object> findSupplierListByPage(Integer page, Integer rows) {

        HashMap<String, Object> goodsMap = new HashMap<>();

        List<Supplier> supplierList = supplierDao.findSupplierListByPage(page , rows) ;

        goodsMap.put("total" , Long.toString(supplierList.size())) ;
        goodsMap.put("rows" , supplierList) ;
        return goodsMap;
    }

    // 分页带条件供应商列表展示
    @Override
    public Map<String, Object> findSuppliersList(Integer page, Integer rows, String supplierName) {

        HashMap<String, Object> goodsMap = new HashMap<>();

        List<Supplier> supplierList = supplierDao.findSuppliersList(page , rows , supplierName) ;

        goodsMap.put("total" , Long.toString(supplierList.size())) ;
        goodsMap.put("rows" , supplierList) ;
        return goodsMap;
    }

    // 保存
    @Override
    public void save(Supplier supplier) {
        supplierDao.save(supplier) ;
    }

    // 更新
    @Override
    public void update(Supplier supplier) {
        supplierDao.update(supplier) ;
    }

    // 删除（可批量）
    @Override
    public void deleteBatch(String ids) {
        //  处理ids (ids: 13,14)
        String[] idArr = ids.split(",");
        for (String idStr : idArr) {
            int id = Integer.parseInt(idStr);
            supplierDao.deleteBatch(id);
        }

    }
}
