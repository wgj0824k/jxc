package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.GoodsDao;
import com.atguigu.jxc.dao.SaleListGoodsDao;
import com.atguigu.jxc.domain.ErrorCode;
import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.domain.SuccessCode;
import com.atguigu.jxc.entity.Goods;
import com.atguigu.jxc.entity.Log;
import com.atguigu.jxc.service.CustomerReturnListGoodsService;
import com.atguigu.jxc.service.GoodsService;
import com.atguigu.jxc.service.LogService;
import com.atguigu.jxc.service.SaleListGoodsService;
import com.atguigu.jxc.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    private GoodsDao goodsDao;
    @Autowired
    private SaleListGoodsDao saleListGoodsDao ;

    @Override
    public ServiceVO getCode() {

        // 获取当前商品最大编码
        String code = goodsDao.getMaxCode();

        // 在现有编码上加1
        Integer intCode = Integer.parseInt(code) + 1;

        // 将编码重新格式化为4位数字符串形式
        String unitCode = intCode.toString();

        for(int i = 4;i > intCode.toString().length();i--){

            unitCode = "0"+unitCode;

        }
        return new ServiceVO<>(SuccessCode.SUCCESS_CODE, SuccessCode.SUCCESS_MESS, unitCode);
    }

    /**
     * 分页查询商品数据
     *
     * @param page 当前页码
     * @param rows 每页行数
     * @return map
     */
    @Override
    public Map<String, Object> findGoodsListByPage(Integer page, Integer rows) {

        HashMap<String, Object> goodsMap = new HashMap<>();
        // 采取goods和goods_type表进行内连接，和t_sale_list_goods进行左连接，分页查询获取
        List<Goods> goodsList = goodsDao.findGoodsListByPage(page , rows) ;
        goodsList.stream().forEach(goods -> {
            if(goods.getSaleTotal() == null) {
                goods.setSaleTotal(0);
            }
        });
        goodsMap.put("total" , Long.toString(goodsList.size())) ;
        goodsMap.put("rows" , goodsList) ;
        return goodsMap;
    }

    /**
     * codeOrName 商品编码/商品名称 采取模糊查询
     * goodsTypeId 商品类别
     *
     */
    @Override
    public Map<String, Object> findGoodsList(Integer page, Integer rows, String codeOrName, String goodsTypeId) {

        HashMap<String, Object> map = new HashMap<>();

        // 采取goods和goods_type表进行内连接，和t_sale_list_goods进行左连接，采取逻辑sql进行查询
        List<Goods> goodsList = goodsDao.findGoodsList(page , rows , codeOrName , goodsTypeId) ;
        goodsList.stream().forEach(goods -> {
            if(goods.getSaleTotal() == null) {
                goods.setSaleTotal(0);
            }
        });

        map.put("total" , Long.toString(goodsList.size())) ;
        map.put("rows" , goodsList) ;
        return map;
    }

    @Override
    public Map<String, Object> findGoods(Integer page, Integer rows, String goodsName, String goodsTypeId) {

        HashMap<String, Object> map = new HashMap<>();

        // 采取goods和goods_type表进行内连接，和t_sale_list_goods进行左连接，采取逻辑sql进行查询
        List<Goods> goodsList = goodsDao.findGoods(page , rows , goodsName , goodsTypeId) ;
        goodsList.stream().forEach(goods -> {
            if(goods.getSaleTotal() == null) {
                goods.setSaleTotal(0);
            }
        });

        map.put("total" , Long.toString(goodsList.size())) ;
        map.put("rows" , goodsList) ;
        return map;
    }

    // 添加
    @Override
    public void save(Goods goods) {

        // TODO 先对inventory_quantity（库存数量），state，last_purchasing_price（上一次采购价格）等先赋值
        goods.setInventoryQuantity(0);
        goods.setLastPurchasingPrice(0);
        goods.setState(0);
        goodsDao.save(goods) ;
    }

    // 修改
    @Override
    public void update(Goods goods) {
//        goods.setInventoryQuantity(0);
//        goods.setLastPurchasingPrice(0);
//        goods.setState(0);
        goodsDao.update(goods) ;
    }

    /**
     *  商品删除（要判断商品状态,入库、有进货和销售单据的不能删除）
     *   商品状态state:0表示初始值,1表示已入库，2表示有进货或销售单据
     * @param goodsId 商品id
     * @return
     */
    @Override
    public ServiceVO delete(Integer goodsId) {

        // 通过id查询goods
        Goods goods = getGoodsById(goodsId);

        // 判断goods的state状态
        Integer state = goods.getState();
        switch (state) {
            case 0:
                goodsDao.delete(goodsId) ;
                return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS) ;
            case 1:
                return new ServiceVO(990 , "商品已入库不能删除") ;
            case 2:
                return new ServiceVO(991 , "商品有进货或销售单据库不能删除") ;
            default:
                return new ServiceVO(999, "未知状态");
        }
    }

    // 通过id查询goods
    private Goods getGoodsById(Integer goodsId) {
        Goods goods = goodsDao.findGoodsById(goodsId) ;
        return goods;
    }

    // 分页查询无库存商品列表：inventory_quantity=0
    @Override
    public Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows) {

        List<Goods> goodsList = goodsDao.getNoInventoryQuantity(page , rows) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("total" , Long.toString(goodsList.size())) ;
        map.put("rows" , goodsList) ;
        return map;
    }

    // 根据名称/编码查询无库存商品列表： inventory_quantity=0
    @Override
    public Map<String, Object> getNoInventoryQuantityByNameOrCode(Integer page, Integer rows, String nameOrCode) {
        List<Goods> goodsList = goodsDao.getNoInventoryQuantityByNameOrCode(page , rows , nameOrCode) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("total" , Long.toString(goodsList.size())) ;
        map.put("rows" , goodsList) ;
        return map;
    }

    // 分页查询有库存商品列表：inventory_quantity>0
    @Override
    public Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows) {
        List<Goods> goodsList = goodsDao.getHasInventoryQuantity(page , rows) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("total" , Long.toString(goodsList.size())) ;
        map.put("rows" , goodsList) ;
        return map;
    }
    // 根据名称/编码查询无库存商品列表： inventory_quantity>0
    @Override
    public Map<String, Object> getHasInventoryQuantityByNameOrCode(Integer page, Integer rows, String nameOrCode) {
        List<Goods> goodsList = goodsDao.getHasInventoryQuantityByNameOrCode(page , rows , nameOrCode) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("total" , Long.toString(goodsList.size())) ;
        map.put("rows" , goodsList) ;
        return map;
    }

    @Override
    public ServiceVO deleteStock(Integer goodsId) {
        // 通过id查询goods
        Goods goods = getGoodsById(goodsId);
        // 设置库存数量为-1
        goods.setInventoryQuantity(-1);

        // 判断goods的state状态
        Integer state = goods.getState();
        switch (state) {
            case 0:
                goodsDao.updateStock(goods);
                return new ServiceVO(SuccessCode.SUCCESS_CODE , SuccessCode.SUCCESS_MESS) ;
            case 1:
                return new ServiceVO(990 , "商品已入库不能删除") ;
            case 2:
                return new ServiceVO(991 , "商品有进货或销售单据库不能删除") ;
            default:
                return new ServiceVO(999, "未知状态");
        }

    }

    // 添加库存（包含inventoryQuantity和purchasingPrice）
    @Override
    public void saveStockOrPurchasingPrice(Integer goodsId ,Integer inventoryQuantity, Double purchasingPrice) {
        goodsDao.saveStockOrPurchasingPrice(goodsId ,inventoryQuantity , purchasingPrice) ;
    }

    // 查询所有 当前库存量 小于 库存下限的商品信息
    // inventory_quantity 当前库存数量 min_num：库存下限
    @Override
    public Map<String, Object> listAlarm() {
        List<Goods> goodsList = goodsDao.listAlarm() ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows" , goodsList) ;
        return map;
    }
}
