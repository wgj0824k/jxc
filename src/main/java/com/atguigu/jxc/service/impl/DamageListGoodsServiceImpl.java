package com.atguigu.jxc.service.impl;

import com.alibaba.fastjson.JSON;
import com.atguigu.jxc.dao.DamageListDao;
import com.atguigu.jxc.dao.DamageListGoodsDao;
import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.service.DamageListGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class DamageListGoodsServiceImpl implements DamageListGoodsService {

    @Autowired
    private DamageListGoodsDao damageListGoodsDao ;
    @Autowired
    private DamageListDao damageListDao ;

    /**
     * 保存报损单：涉及两张表：t_damage_list和t_damage_list_goods
     *
     * @param damageNumber 报损单号（一个报损单列表对应一个报损单号）
     * @param damageList 报损单列表实体类
     * @param damageListGoodsStr damageListGoods的json格式
     *
     * @return
     */
    @Override
    @Transactional
    public void save(String damageNumber, DamageList damageList, String damageListGoodsStr) {
        List<DamageListGoods> damageListGoods = JSON.parseArray(damageListGoodsStr, DamageListGoods.class);
        // 参数赋值
        // userId采取管理员1
        damageList.setUserId(1);
        damageList.setDamageNumber(damageNumber);

        // 分开两张表进行保存
        damageListDao.save(damageList) ;
        // 此处采取配置mybatis的插入后回显主键，避免再次查询数据库
//        DamageList damageListSave = damageListDao.findByDamageNumber(damageNumber) ;
        for (DamageListGoods damageListGood : damageListGoods) {
            damageListGood.setDamageListId(damageList.getDamageListId());
            damageListGoodsDao.save(damageListGood) ;
        }
    }

    /**
     * 报损单查询
     *
     * @param sTime 开始时间
     * @param eTime 结束时间
     * @return
     */
    @Override
    public Map<String, Object> list(String sTime, String eTime) {
       List<DamageList> damageLists = damageListDao.list(sTime , eTime) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows" , damageLists) ;
        return map;
    }

    /**
     * 查询报损单商品信息
     *
     * @param damageListId 报损单Id
     * @return map
     */
    @Override
    public Map<String, Object> goodsList(Integer damageListId) {

        List<DamageListGoods> damageListGoodsList = damageListGoodsDao.goodsList(damageListId) ;
        HashMap<String, Object> map = new HashMap<>();
        map.put("rows" , damageListGoodsList) ;
        return map;
    }
}
