package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface CustomerService {

    Map<String, Object> findCustomerListByPage(Integer page, Integer rows);

    Map<String, Object> findCustomerList(Integer page, Integer rows, String customerName);

    void save(Customer customer);

    void update(Customer customer);

    void deleteBatch(String ids);

}
