package com.atguigu.jxc.service;

import com.atguigu.jxc.entity.Supplier;

import java.util.Map;

public interface SupplierService {

    Map<String, Object> findSupplierListByPage(Integer page, Integer rows);

    Map<String, Object> findSuppliersList(Integer page, Integer rows, String supplierName);

    void save(Supplier supplier);

    void update(Supplier supplier);

    void deleteBatch(String ids);

}
