package com.atguigu.jxc.service;

import com.atguigu.jxc.domain.ServiceVO;
import com.atguigu.jxc.entity.Goods;

import java.util.Map;

public interface GoodsService {


    ServiceVO getCode();

    Map<String, Object> findGoodsListByPage(Integer page, Integer rows);

    Map<String, Object> findGoodsList(Integer page, Integer rows, String codeOrName, String goodsTypeId);

    Map<String, Object> findGoods(Integer page, Integer rows, String goodsName, String goodsTypeId);

    void save(Goods goods);

    void update(Goods goods);

    ServiceVO delete(Integer goodsId);

    Map<String, Object> getNoInventoryQuantity(Integer page, Integer rows);

    Map<String, Object> getNoInventoryQuantityByNameOrCode(Integer page, Integer rows, String nameOrCode);

    Map<String, Object> getHasInventoryQuantity(Integer page, Integer rows);

    Map<String, Object> getHasInventoryQuantityByNameOrCode(Integer page, Integer rows, String nameOrCode);

    ServiceVO deleteStock(Integer goodsId);

    void saveStockOrPurchasingPrice(Integer goodsId ,Integer inventoryQuantity, Double purchasingPrice);

    Map<String, Object> listAlarm();

}
