
package com.atguigu.jxc.config;

import io.swagger.annotations.ApiOperation;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.*;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2WebMvc;

import java.util.ArrayList;
import java.util.List;


@Configuration
@Import(BeanValidatorPluginsConfiguration.class)
@EnableSwagger2WebMvc
public class SwaggerConfiguration implements WebMvcConfigurer {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
    }

    @Bean(value = "docket")
    public Docket docket() {
        // 配置全局参数token
        List<Parameter> pars = new ArrayList<>();
        ParameterBuilder tokenPar = new ParameterBuilder();
        tokenPar.name("token")
                .description("用户token")
                .defaultValue("")
                .modelRef(new ModelRef("string"))
                .parameterType("header")
                .required(false)
                .build();
        pars.add(tokenPar.build());

        Docket docket=new Docket(DocumentationType.SWAGGER_2)
                // 配置基本信息
                .apiInfo(apiInfo())
                // 配置分组组名
                .groupName("jxc接口")
                // 配置接口信息，设置扫描接口
                .select()
                // 扫描路径下的包的生成接口文档
                .apis(RequestHandlerSelectors.basePackage("com.atguigu.jxc.controller"))
                // 加了 RestController 注解的类，才生成接口文档
                .apis(RequestHandlerSelectors.withClassAnnotation(RestController.class))
                /**
                 * PathSelectors
                 *      .any() // 满足条件的路径，该断言总为true
                 *      .none() // 不满足条件的路径，该断言总为false（可用于生成环境屏蔽 swagger）
                 *      .ant("/user/**") // 满足字符串表达式路径
                 *      .regex("") // 符合正则的路径
                 */
                .paths(PathSelectors.any())
                .build()
                // 将API的基本路径映射为根路径"/"
                .pathMapping("/")
                // 配置全局参数
                .globalOperationParameters(pars);
        return docket;
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("进销存knife4j接口文档测试")
                .description("众里寻他千百度，慕然回首那人却在灯火阑珊处")
                .contact(new Contact("王港军" ,"www.wgj.com" ,"wgj0824@163.com" ))
                .termsOfServiceUrl("https://stackoverflow.com/")
                .license("Apache 2.0")
                .licenseUrl("https://stackoverflow.com/")
                .version("v4.0")
                .build();
    }
}
