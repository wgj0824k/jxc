package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageListGoodsDao {
    void save(DamageListGoods damageListGoods);

    List<DamageListGoods> goodsList(Integer damageListId);
}
