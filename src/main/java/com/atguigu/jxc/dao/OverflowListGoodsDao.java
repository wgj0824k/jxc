package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageListGoods;
import com.atguigu.jxc.entity.OverflowListGoods;

import java.util.List;

public interface OverflowListGoodsDao {
    void save(OverflowListGoods overflowListGoods);

    List<OverflowListGoods> goodsList(Integer overflowListId);

}
