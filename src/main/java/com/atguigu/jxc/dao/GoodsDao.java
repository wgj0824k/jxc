package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Goods;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @description 商品信息
 */
public interface GoodsDao {


    String getMaxCode();


    List<Goods> findGoodsListByPage(Integer page, Integer rows);

    List<Goods> findGoodsList(Integer page, Integer rows, String codeOrName, String goodsTypeId);

    List<Goods> findGoods(Integer page, Integer rows, String goodsName, String goodsTypeId);

    void save(Goods goods);

    void update(Goods goods);

    Goods findGoodsById(Integer goodsId);

    void delete(Integer goodsId);

    List<Goods> getNoInventoryQuantity(Integer page, Integer rows);

    List<Goods> getNoInventoryQuantityByNameOrCode(Integer page, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantityByNameOrCode(Integer page, Integer rows, String nameOrCode);

    List<Goods> getHasInventoryQuantity(Integer page, Integer rows);

    void updateStock(Goods goods);

    void saveStockOrPurchasingPrice(Integer goodsId, Integer inventoryQuantity, Double purchasingPrice);

    List<Goods> listAlarm();

}
