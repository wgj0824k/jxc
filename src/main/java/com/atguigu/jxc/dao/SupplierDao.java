package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SupplierDao {

    List<Supplier> findSupplierListByPage(Integer page, Integer rows);

    List<Supplier> findSuppliersList(Integer page, Integer rows, String supplierName);

    void save(Supplier supplier);

    void update(Supplier supplier);

    void deleteBatch(@Param("supplierId") Integer id);
}
