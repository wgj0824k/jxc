package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.Customer;
import com.atguigu.jxc.entity.Supplier;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CustomerDao {

    List<Customer> findCustomerListByPage(Integer page, Integer rows);

    List<Customer> findCustomerList(Integer page, Integer rows, String customerName);

    void save(Customer customer);

    void update(Customer customer);

    void deleteBatch(@Param("customerId") Integer id);
}
