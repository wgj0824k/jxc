package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.OverflowList;

import java.util.List;

public interface OverflowListDao {
    void save(OverflowList overflowList);

    OverflowList findByOverflowNumber(String overflowNumber);

    List<OverflowList> list(String sTime, String eTime);
}
