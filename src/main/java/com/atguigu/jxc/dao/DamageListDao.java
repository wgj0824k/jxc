package com.atguigu.jxc.dao;

import com.atguigu.jxc.entity.DamageList;
import com.atguigu.jxc.entity.DamageListGoods;

import java.util.List;

public interface DamageListDao {
    void save(DamageList damageList);

    DamageList findByDamageNumber(String damageNumber);

    List<DamageList> list(String sTime, String eTime);

    List<DamageListGoods> goodsList(Integer damageListId);

}
