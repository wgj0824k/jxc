package com.atguigu.jxc.entity;

import lombok.Data;
/**
 * 商品信息实体
 */
@Data
public class Goods {

  private Integer goodsId;
  private String goodsCode;
  private String goodsName;
  private Integer inventoryQuantity;
  private double lastPurchasingPrice;
  private Integer minNum;
  private String goodsModel;
  private String goodsProducer;
  private double purchasingPrice;
  private String remarks;
  private double sellingPrice;
  private Integer state;// 0 初始化状态 1 期初库存入仓库  2  有进货或者销售单据
  private String goodsUnit;
  private Integer goodsTypeId;

  // 来源于t_goods_type表
  private String goodsTypeName;

  // 来源于t_sale_list_goods表：t_sale_list_goods - t_customer_return_list_goods
  private Integer saleTotal;// 销售总量

}
